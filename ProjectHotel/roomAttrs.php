<?php
session_start();

//cheak out client
if(!isset($_SESSION['userid'])){
	echo "<script language=\"javascript\">window.open(\"index.html\", \"_parent\");</script>";
	exit();
}
?>
<html>
<head>
<?php 
	$id="";
	if(isset($_GET['id']) && $_GET['id'] != "")
		$id = $_GET['id'];
 ?>
	<meta charset="UTF-8">
	<title>Room Attributes</title>
	<h1 style="text-align: center;">Room<?php echo "$id"; ?> Attributes</h1>
	<script src="tricks/jquery-1.11.0.min.js"></script>
	<script src="tricks/jquery-migrate-1.2.1.js"></script>
	<script src="tricks/jquery.form.js"></script>
	<link rel="stylesheet" type="text/css" href="tricks/jquery-ui-1.10.4.custom/css/smoothness/jquery-ui-1.10.4.custom.min.css">
	<script src="tricks/jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
</head>
<body>
<?php 
	if ($id != "") {
		include 'connect.php';
		$sql = "select idRoom, type, price, state from Room where idRoom= $id limit 1";
		$res = mysqli_query($connect,$sql);
		if ($row = mysqli_fetch_array($res,MYSQL_ASSOC)) {
			$id = $row['idRoom'];
			$type = $row['type'];
			$price = $row['price'];
			$state = $row['state'];
		}
	}
	
 ?>
 <form id="formUpdate" name="formRoom" method="post" action="#">
 	<input type="hidden" name="formName" value="Room">
 	<input type="text" name="ID" id="ID" value=<?php echo $id; ?> readonly><label for="ID">ID</label><br>
 	<input type="text" name="type" id="type" value=<?php echo $type; ?> ><label for="type">Type</label><br>
 	<input type="text" name="price" id="price" value=<?php echo $price; ?>><label for="price">Price</label><br>
 	<input type="text" name="state" id="state" value=<?php echo $state; ?>><label for="state">state</label><br>
 	<div class="update"><button type="submit" id="update" name="update" value="Update" onclick="CKupdate();">Update</button>
 		<a class="back" href="javascript:history.back(-1);">Back</a>
 	</div>
 </form>	

 <span id='response'></span>
<script>
$(function() {

	$( "input[type=submit], a, button" ).button();

	var options = {
		url: "submitList.php", 
		target: "#response",
					// success: showResponse 
		// success: function () {
		// 	window.location.reload();
		// } 
	};	

	
	$("#formUpdate").ajaxForm(options);

				
});
	
</script>
</body>
</html>
