<?php
$config=array();

$config['type']=array("flash","img");	

$config['img']=array("jpg","bmp","gif","png");	
$config['flash']=array("flv","swf");	

$config['flash_size']=20000;	
$config['img_size']=5000;	

$config['message']="uploaded successfully!";	

$config['name']=mktime();	

$config['flash_dir']="/image/flash";	
$config['img_dir']="/image/events_news";	

$config['site_url']="";	


uploadfile();

function uploadfile(){
	global $config;
	
	if(!empty($_GET['CKEditorFuncNum'])){
		//mkhtml(1,"","function call error");

		$fn=$_GET['CKEditorFuncNum'];
		
		if(is_uploaded_file($_FILES['upload']['tmp_name'])){
			$fileAds = $_FILES['upload']['tmp_name'];

			if(!in_array($_GET['type'],$config['type']))
				mkhtml($fn,"","file call error");
			$type=$_GET['type'];
		//check the file
			$fileArr=pathinfo($_FILES['upload']['name']);
			$fileType=$fileArr["extension"];
			if(!in_array($fileType,$config[$type]))
				mkhtml($fn,"","type of file illegal");
		//check the size
			if($_FILES['upload']['size']>$config[$type."_size"]*1024)
				mkhtml($fn,"","maximum size is ".$config[$type."_size"]."KB!");
		//$fileArr=explode(".",$_FILES['upload']['name']);
		//$fileType=$fileArr[count($fileArr)-1];
			$file_target=$config[$type."_dir"]."/".$config['name'].".".$fileType;
			$file_host=dirname($_SERVER['DOCUMENT_ROOT']."/ProjectHotel/").$file_target;

			$upload_dir = realpath(dirname($_SERVER['SCRIPT_FILENAME']).'/../').$file_target;
			//for localhost test
			// $upload_url = 'http://'.$_SERVER['HTTP_HOST'].'/Projetdev';
			// for real web server
			$upload_url = 'http://'.$_SERVER['HTTP_HOST'];

			if(move_uploaded_file($_FILES['upload']['tmp_name'],$upload_dir)){
				mkhtml($fn,$upload_url.$file_target,$config['message']);
			}else{
				mkhtml($fn,""," uploaded unsuccessfully, please check the directory permissions: $upload_dir");
			}
		}
	}
}
//output js
function mkhtml($fn,$fileUrl,$message){
	$str='<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction('.$fn.', \''.$fileUrl.'\', \''.$message.'\');</script>';
	exit($str);
}
?>
