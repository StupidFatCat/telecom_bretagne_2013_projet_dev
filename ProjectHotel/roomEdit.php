<?php
session_start();

//cheak out client
if(!isset($_SESSION['userid'])){
	echo "<script language=\"javascript\">window.open(\"index.html\", \"_parent\");</script>";
	exit();
}
?>
<!DOCTYPE html>
<html>	
	<head>
		<title>A Simple Page with CKEditor</title>
		<script src="tricks/jquery-1.11.0.min.js"></script>
		<script src="ckeditor/ckeditor.js"></script>
		<script src="tricks/jquery.form.js"></script>
		<script src="tricks/jquery.validate.js"></script>		
		<link rel="stylesheet" type="text/css" href="tricks/jquery-ui-1.10.4.custom/css/smoothness/jquery-ui-1.10.4.custom.min.css">
		<script src="tricks/jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.min.js"></script>
		<link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
	</head>
	<body>
<?php
	$id='1';
	$langue='_fr';
	$titleLang = 'French';
if ($_GET['id'] != "") {
	$id = $_GET['id'];
	if($_GET['langue'] == "_en"){
		$langue = $_GET['langue'];
		$titleLang = 'English';
	}
}

	include 'connect.php';
	//langue?
	if(strlen($id) == 1)
		$sql = "select description$langue from  Room where idRoom = $id limit 1";
	else if($id == 'sdt')
		$sql = "select description$langue from Salon where idSalon = 1 limit 1";
	else if($id == 'menu')
		$sql = "select menu from Salon where idSalon = 1 limit 1";
	else if($id == 'house')
		$sql = "select description$langue from House where idHouse = 1 limit 1";
	
	$res = mysqli_query($connect,$sql);
	if ($row = mysqli_fetch_array($res,MYSQL_ASSOC)) {
		if($id != 'menu')
			$description = $row['description'.$langue];
		else 
			$description = $row['menu'];
	}

 ?>
		<h1><?php 
		if(strlen($id) == 1)
			echo "Room$id $titleLang"; 
		else if($id == 'sdt')
			echo "Salon $titleLang";
		else if($id == 'menu')
			echo "Menu";
		else if($id == 'house')
			echo "House $titleLang";

		?> Description</h1>
		<form id="editRoom" method="post" action="#">

			<p>
				<textarea name="editor1" id="editor1" rows="10" cols="80">
					<?php	echo "$description"; ?>
				</textarea>
				<script>
					CKEDITOR.replace( 'editor1' );
				</script>
			</p>
			<div class="submit">
				<input type="submit" value=Submit name="submit" onclick="CKupdate();"/>
				<a href="javascript:history.back(-1);">Back</a>
			</div>
			<p>
				<span id="response" style="color:red"></span>
			</p>

		</form>
		<script type="text/javascript">
			$(function() {
				$( "input[type=submit], a, button" ).button();
		
				var options = {
					url: <?php 
						echo '"roomSubmit.php?id='."$id&langue=$langue".'"';
					?>, 
					target: "#response",
					// success: showResponse 
				};
				$("#editRoom").ajaxForm(options);

				
			});

			function CKupdate(){
				for ( instance in CKEDITOR.instances )
					CKEDITOR.instances[instance].updateElement();
			}		
			function showResponse(responseText, statusText)  { 
			    // for normal html responses, the first argument to the success callback 
			    // is the XMLHttpRequest object's responseText property 
			 
			    // if the ajaxForm method was passed an Options Object with the dataType 
			    // property set to 'xml' then the first argument to the success callback 
			    // is the XMLHttpRequest object's responseXML property 
			 
			    // if the ajaxForm method was passed an Options Object with the dataType 
			    // property set to 'json' then the first argument to the success callback 
			    // is the json data object returned by the server 
			 
			    alert('status: ' + statusText + '\n\nresponseText: \n' + responseText + 
			        '\n\nThe output div should have already been updated with the responseText.'); 
			} 
		</script>
	</body>
</html>