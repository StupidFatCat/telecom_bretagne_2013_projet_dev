<?php
session_start();

//cheak out client
if(!isset($_SESSION['userid'])){
	echo "<script language=\"javascript\">window.open(\"index.html\", \"_parent\");</script>";
	exit();
}
?>
<!DOCTYPE html>
<html>	
	<head>
		<title>A Simple Page with CKEditor</title>
		<script src="tricks/jquery-1.11.0.min.js"></script>
		<script src="ckeditor/ckeditor.js"></script>
		<script src="tricks/jquery.form.js"></script>
		<script src="tricks/jquery.validate.js"></script>		
		<link rel="stylesheet" type="text/css" href="tricks/jquery-ui-1.10.4.custom/css/smoothness/jquery-ui-1.10.4.custom.min.css">
		<script src="tricks/jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.min.js"></script>
		<link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
	</head>
	<body>
<?php
$isUpdate = false; 
if ($_GET['id'] != "") {
	$id = $_GET['id'];
	if($_GET['update'] == "true"){
		$isUpdate = true;
	}
}

if ($isUpdate) {
	include 'connect.php';
	$sql = "select * from OtherDescriptions where idOtherDescriptions = $id limit 1";
	$res = mysqli_query($connect,$sql);
	if ($row = mysqli_fetch_array($res,MYSQL_ASSOC)) {
		$name = $row['name'];
		$description = $row['description'];
		$link = $row['link'];
	}
}

 ?>
		<h1><?php
		 if($isUpdate)
		 	echo "Update ";
		 else
		 	echo "New ";
		 ?>Description</h1>
		<form id="editDes" method="post" action="#">
			<p>
				<span class="edit">Name:</span><input type="text" id="name" name="name"<?php if ($isUpdate) {
					echo "value='$name'";
				} ?>/> <br>
			</p>
			<p>
				<span class="edit">Link:</span><input style="width: 40%;" type="url" id="link" name="link"<?php if ($isUpdate) {
					echo "value='$link'";
				} ?>/> <br>
			</p>

			<p>
				<textarea name="editor1" id="editor1" rows="10" cols="80">
					<?php if ($isUpdate) {
						echo "$description";
					} ?>
				</textarea>
				<script>
					CKEDITOR.replace( 'editor1' );
				</script>
			</p>
			<div class="submit">
				<input type="submit" value=<?php if ($isUpdate) {
					echo "Update";
				}else {
					echo "Submit";
				} ?> name="submit" onclick="CKupdate();"/>
				<a href="javascript:history.back(-1);">Back</a>
			</div>
			<p>
				<span id="response" style="color:red"></span>
			</p>

		</form>
		<script type="text/javascript">
			$(function() {
				$( "#radioLangue" ).buttonset();
				$( "input[type=submit], a, button" ).button();
					// .button()
					// .click(function( event ) {
					// 	// event.preventDefault();
					// });
				// $("#editDes").validate({
				// 	rules: {
				// 		title: "required",
				// 	},
				// 	messages: {
				// 		title: "Title is required.",
				// 	},
				// 	errorPlacement: function (error, element) {
				// 		if(element.is("#title")) 
    //                 		error.appendTo("#err_title");
				// 	},
				// 	submitHandler: function(form){
				// 		var options = {
				// 			url: "editSubmit.php?id=news", 
				// 			target: "#response",
				// 			success: showResponse 
				// 		};	
				// 		$(form).ajaxForm(options);
				// 	} 	

				// });
				var options = {
					url: <?php if ($isUpdate) {
						echo '"editSubmit.php?mode=des&update=true&id='.$id.'"';
					} else {
						echo '"editSubmit.php?mode=des&update=false"';
					} ?>, 
					target: "#response",
					// success: showResponse 
				};
				$("#editDes").ajaxForm(options);

				
			});

			function CKupdate(){
			for ( instance in CKEDITOR.instances )
				CKEDITOR.instances[instance].updateElement();
			}	
function showResponse(responseText, statusText)  { 
			    // for normal html responses, the first argument to the success callback 
			    // is the XMLHttpRequest object's responseText property 
			 
			    // if the ajaxForm method was passed an Options Object with the dataType 
			    // property set to 'xml' then the first argument to the success callback 
			    // is the XMLHttpRequest object's responseXML property 
			 
			    // if the ajaxForm method was passed an Options Object with the dataType 
			    // property set to 'json' then the first argument to the success callback 
			    // is the json data object returned by the server 
			 
			    alert('status: ' + statusText + '\n\nresponseText: \n' + responseText + 
			        '\n\nThe output div should have already been updated with the responseText.'); 
			} 
		</script>
	</body>
</html>