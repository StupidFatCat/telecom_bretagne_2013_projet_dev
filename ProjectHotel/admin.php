<?php
session_start();

//cheak out client
if(!isset($_SESSION['userid'])){
	echo "<script language=\"javascript\">window.open(\"index.html\", \"_parent\");</script>";
	exit();
}

include('connect.php');
$userid = $_SESSION['userid'];
$username = $_SESSION['username'];
$user_query = mysqli_query($connect,"select * from Admin where idAdmin=$userid limit 1");
$row = mysqli_fetch_array($user_query);

?>
<!DOCTYPE html>
<html>
<head>
	<title>Hotel Admin</title>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<script src="tricks/jquery-1.11.0.min.js"></script>
	<script src="tricks/jquery-migrate-1.2.1.js"></script>
	<script>
	$(function() {
		$("div.nav_h").animate({ opacity: '.7'}, 700, function() {
			$("div.acco").animate({ opacity: '1'}, 700, function() {
				$(".frame_main").animate({ opacity: '1'}, 700, function() {});
			});
		});
		
	});
	</script>
</head>
<body>
	<?php include 'tricks/nav_h.html'; ?>
	<?php // include 'tricks/nav_v.html'; ?>
	<div class="acco">
		<?php include 'tricks/accordion.html'  ?>
	</div>
	<iframe src="reservation.php" class="frame_main" name="frame_main" scrolling="no" ></iframe>
</body>
</html>