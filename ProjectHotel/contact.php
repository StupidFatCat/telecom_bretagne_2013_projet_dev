<?php
session_start();

//cheak out client
if(!isset($_SESSION['userid'])){
	echo "<script language=\"javascript\">window.open(\"index.html\", \"_parent\");</script>";
	exit();
}
?>
<html>
<head>
<?php 
	$id="";
	if(isset($_GET['id']) && $_GET['id'] != "")
		$id = $_GET['id'];
 ?>
	<meta charset="UTF-8">
	<title>Contact</title>
	<h1 style="text-align: center;">Contact</h1>
	<script src="tricks/jquery-1.11.0.min.js"></script>
	<script src="tricks/jquery-migrate-1.2.1.js"></script>
	<script src="tricks/jquery.form.js"></script>
	<link rel="stylesheet" type="text/css" href="tricks/jquery-ui-1.10.4.custom/css/smoothness/jquery-ui-1.10.4.custom.min.css">
	<script src="tricks/jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
</head>
<body>
<?php 
		include 'connect.php';
		$sql = "select name, address, postalCode, city, tel, mail from Contact where idContact= 1 limit 1";
		$res = mysqli_query($connect,$sql);
		if ($row = mysqli_fetch_array($res,MYSQL_ASSOC)) {
			$name = $row['name'];
			$address = $row['address'];
			$postalCode = $row['postalCode'];
			$city = $row['city'];
			$tel = $row['tel'];
			$mail = $row['mail'];
		}

	
 ?>
 <form id="formUpdate" name="formContact" method="post" action="#">
 	<input type="hidden" name="formName" value="Contact">
 	<input type="text" name="name" id="name" value=<?php echo $name; ?> ><label for="name">Name</label><br>
 	<input type="text" name="address" id="address" value=<?php echo $address; ?>><label for="address">Address</label><br>
 	<input type="text" name="postalCode" id="postalCode" value=<?php echo $postalCode; ?>><label for="postalCode">PostcalCode</label><br>
 	<input type="text" name="city" id="city" value=<?php echo $city; ?>><label for="city">City</label><br>
 	<input type="tel" name="tel" id="tel" value=<?php echo $tel; ?>><label for="tel">Tel</label><br>
 	<input type="email" name="mail" id="mail" value=<?php echo $mail; ?>><label for="mail">Mail</label><br>
 	<div class="update"><button type="submit" id="update" name="update" value="Update" onclick="CKupdate();">Update</button>
 		<a class="back" href="javascript:history.back(-1);">Back</a>
 	</div>
 </form>	

 <span id='response'></span>
<script>
$(function() {

	$( "input[type=submit], a, button" ).button();

	var options = {
		url: "submitList.php", 
		target: "#response",
					// success: showResponse 
		// success: function () {
		// 	window.location.reload();
		// } 
	};	

	
	$("#formUpdate").ajaxForm(options);

				
});
	
</script>
</body>
</html>
