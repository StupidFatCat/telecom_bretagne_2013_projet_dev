<?php
session_start();

//cheak out client
if(!isset($_SESSION['userid'])){
	echo "<script language=\"javascript\">window.open(\"index.html\", \"_parent\");</script>";
	exit();
}
?>
<html>
<head>
	<meta charset="UTF-8">
	<title>Comments</title>
	<h1 style="text-align: center;">Comments</h1>

	<link rel="stylesheet" href="css/table.css">
	<script src="tricks/jquery-1.11.0.min.js"></script>
	<script src="tricks/jquery-migrate-1.2.1.js"></script>
	<script src="js/tableCheckbox.js"></script>
	<script src="tricks/jquery.form.js"></script>
	<link rel="stylesheet" type="text/css" href="tricks/jquery-ui-1.10.4.custom/css/smoothness/jquery-ui-1.10.4.custom.min.css">
	<script src="tricks/jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.min.js"></script>
	<!-- <link rel="stylesheet" type="text/css" href="css/jquery-ui.css"> -->


	<?php 
	include 'connect.php'; 
	?>
</head>
<body>
<form id="formOrder" method="post" action="comment.php">
	
	<div id="radio">
		<button type="submit" id="search" value="Search">Search</button>
		<span>&nbsp;Order By:&nbsp;</span>
		<input type="radio" id="radio1" name="order" value="idComment" /><label for="radio1">ID</label>
		<input type="radio" id="radio2" name="order" value="name" /><label for="radio2">Name</label>
		<input type="radio" id="radio3" name="order" value="date" /><label for="radio3">Date</label>
		<input type="radio" id="radio4" name="order" value="valid" /><label for="radio4">valid</label>
	</div>
	
</form>
<form id="formCom" method="post" action="submitList.php">
	<input type="hidden" name="formName" value="Comment">
<?php
require 'Pager.php';
//
class CommentPager extends Pager {
	public function showCommentList() {
		$data = $this->getPageData ();
		$form = "
		<table>
		<tr>
			<th><input id='pickAll' type='checkbox' value='0'/></th>
			<th>ID</th>
			<th>Name</th>
			<th>Date</th>
			<th>Valid</th>
			<th></th>
		</tr>
		";
		// 显示结果的代码
		// ......
		echo "$form";

		foreach ( $data as $row ) {
			$id = $row['idComment'];
			echo "<tr>";
			echo "<td><input name='chkItem[]' type='checkbox' value=$id></td>";
			foreach ( $row as $key => $value ) {
				if($key != 'content'){
					echo "<td id=$id class=$key value=$value>";
					echo $value;
					echo "</td>";
				}
			}
			echo "
			<td>
				<button class='update' name='update' type='button' value=".$row['idComment'].">
					<span class='ui-icon ui-icon-gear'></span>
				</button>
			</td>";
			echo "</tr>";
		}
		echo "</table>";
	}

	public function outputComment() {
		$data = $this->getPageData();

		foreach ($data as $row) {
			$id=$row['idComment'];
			$content = $row['content'];
			echo "
			<div id='dialog-valid-$id' class='dialog-valid' value='$id' title='Valid this content'>
				<form id=$id class='formComment' method='post' action='submitList.php'>
					<div id='dialog-confirm'>
						<p>$content</p>
					</div> 
					<input type='hidden' name='valid' value=$id />
					<span id='response-$id' style='color:red;'></span>
				</form>
			</div>
			";
		}
	}
}

// / 调用
if (isset ( $_GET ['page'] )) {
	$page = ( int ) $_GET ['page'];
} else {
	$page = 1;
}
if (isset($_POST['order'])){
	$sql = "select * from Comment order by ".$_POST['order'];
} else {
	$sql = "select * from Comment order by idComment";
}
$pager_option = array (
		"sql" => $sql,
		"PageSize" => 10,
		"CurrentPageID" => $page 
);
if (isset ( $_GET ['numItems'] )) {
	$pager_option ['numItems'] = ( int ) $_GET ['numItems'];
}
//include 'connect.php';

 $pager = new CommentPager ( $pager_option );
 $pager->showCommentList ();
if ($pager->isFirstPage) {
	$turnover = "<a class='pager'>FirstPage</a><a class='pager'>Prev</a>";
} else {
	$turnover = "<a class='pager' href='?page=1&numItems=" . $pager->numItems . "'>FirstPage</a><a class='pager' href='?page=" . $pager->PreviousPageID . "&numItems=" . $pager->numItems . "'>Prev</a>";
}
if ($pager->isLastPage) {
	$turnover .= "<a class='pager'>Next</a><a class='pager'>LastPage</a>";
} else {
	$turnover .= "<a class='pager' href='?page=" . $pager->NextPageID . "&numItems=" . $pager->numItems . "'>Next</a>|<a class='pager' href='?page=" . $pager->numPages . "&numItems=" . $pager->numItems . "'>LastPage</a>";
}
?>


<div class="delete">
	<input type="submit" id="delete" name="delete" value="Delete">
	<span id="responseCom" style="color:red;"></span>
</div>
</form>
<div class="pager">
	<p>
	<?php echo $turnover; ?>
	</p>
	<p>
		<?php echo "$pager->CurrentPageID / $pager->numPages"; ?>
	</p>
</div>

<?php 
$pager->outputComment();
 ?>
<script>
	$(function() {
		var allFields = $( [] );
		// var name = $( "#name" ),
		// 	email = $( "#email" ),
		// 	password = $( "#password" ),
		// 	allFields = $( [] ).add( name ).add( email ).add( password ),
		// 	tips = $( ".validateTips" );

		$( "input[type=submit], a, button" ).button();
		$( "#radio" ).buttonset();	

		var options = {
			url: "submitList.php", 
			target: "#responseCom",
						// success: showResponse 
			success: function () {
				window.location.reload();
			} 
		};
		

		$( ".dialog-valid" ).dialog({
			autoOpen: false,
			// height: 600,
			width: 450,
			modal: true,
			position: [333, 150],
			buttons: {
				"Validate": function() {
					var bValid = true, id;
					allFields.removeClass( "ui-state-error" );
					id = $(this).attr("value");
					var options2 = {
						url: "submitList.php?valid=true", 
						target: "#response-"+id,
									// success: showResponse 
						// success: function () {
						// 	window.location.reload();
						// } 
					};	
					$("#"+id+".formComment").ajaxSubmit(options2);
				},
				"Invalidate": function() {
					var bValid = true, id;
					allFields.removeClass("ui-state-error");
					id = $(this).attr("value");
					var options3 = {
						url: "submitList.php?valid=false", 
						target: "#response-"+id,
					}
					$("#"+id+".formComment").ajaxSubmit(options3);
				},
				Cancel: function() {
					$( this ).dialog( "close" );
					window.location.reload();
					
				}
			},
			close: function() {
				allFields.val( "" ).removeClass( "ui-state-error" );
				window.location.reload();
			}
		});	


		$("button.update").click(function(){
			var idInput = $(this).val();
			// $("input.text").each(function(){
			// 	var name = $(this).attr("id");
			// 	var str = "td#"+idInput+"."+name;
			// 	var value = $(str).text();
			// 	$(this).val(value);
			// });
			$("#dialog-valid-"+idInput).dialog("open");
			
		});


		$("#formCom").ajaxForm(options);	
	});
	
</script>
</body>
</html>
