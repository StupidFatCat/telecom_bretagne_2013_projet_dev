<?php
session_start();

//cheak out client
if(!isset($_SESSION['userid'])){
	echo "<script language=\"javascript\">window.open(\"index.html\", \"_parent\");</script>";
	exit();
}
?>

<html>
<head>
	<meta charset="UTF-8">
	<title>Reservations</title>
	<h1 style="text-align: center;">Reservations</h1>

	<link rel="stylesheet" href="css/table.css">
	<script src="tricks/jquery-1.11.0.min.js"></script>
	<script src="tricks/jquery-migrate-1.2.1.js"></script>
	<script src="js/tableCheckbox.js"></script>
	<script src="tricks/jquery.form.js"></script>
	<link rel="stylesheet" type="text/css" href="tricks/jquery-ui-1.10.4.custom/css/smoothness/jquery-ui-1.10.4.custom.min.css">
	<script src="tricks/jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.min.js"></script>
	<!-- <link rel="stylesheet" type="text/css" href="css/jquery-ui.css"> -->


	<?php 
	include 'connect.php'; 
	?>
</head>
<body>
<form id="formOrder" method="post" action="reservation.php">
	
	<div id="radio">
		<button type="submit" id="search" value="Search">Search</button>
		<span>&nbsp;Order By:&nbsp;</span>
		<input type="radio" id="radio1" name="order" value="idReservation" /><label for="radio1">ID</label>
		<input type="radio" id="radio2" name="order" value="name" /><label for="radio2">Name</label>
		<input type="radio" id="radio3" name="order" value="firstname" /><label for="radio3">Firstname</label>
		<input type="radio" id="radio4" name="order" value="sex" /><label for="radio4">Sex</label>
		<input type="radio" id="radio5" name="order" value="dateIn" /><label for="radio5">DateIn</label>
		<input type="radio" id="radio6" name="order" value="dateOut" /><label for="radio6">DateOut</label>
		<input type="radio" id="radio7" name="order" value="price" /><label for="radio7">Price</label>
		<input type="radio" id="radio8" name="order" value="Room_idRoom" /><label for="radio8">Room</label>
		<input type="radio" id="radio9" name="order" value="dateRes" /><label for="radio9">OperationTime</label>
		<input type="radio" id="radio10" name="order" value="state" /><label for="radio10">state</label>
		<input type="radio" id="radio11" name="order" value="mail" /><label for="radio11">Mail</label>
		<input type="radio" id="radio12" name="order" value="phone" /><label for="radio12">Phone</label>
	</div>
	
</form>
<form id="formRes" method="post" action="submitList.php">
	<input type="hidden" name="formName" value="Reservation">
<?php
require 'Pager.php';
//
class ReservationPager extends Pager {
	public function showReservationList() {
		$data = $this->getPageData ();
		$form = "
		<table>
		<tr>
			<th><input id='pickAll' type='checkbox' value='0'/></th>
			<th>ID</th>
			<th>Name</th>
			<th>Firstname</th>
			<th>Sex</th>
			<th>DateIn</th>
			<th>DateOut</th>
			<th>Price</th>
			<th>Room</th>
			<th>OperationTime</th>
			<th>State</th>
			<th>Mail</th>
			<th>Phone</th>
			<th></th>
		</tr>
		";
		// 显示结果的代码
		// ......
		echo "$form";

		foreach ( $data as $row ) {
			$id = $row['idReservation'];
			echo "<tr>";
			echo "<td><input name='chkItem[]' type='checkbox' value=$id></td>";
			foreach ( $row as $key => $value ) {
				echo "<td id=$id class=$key value=$value>";
				echo $value;
				echo "</td>";
			}
			echo "
			<td>
				<button class='update' name='update' type='button' value=".$row['idReservation'].">
					<span class='ui-icon ui-icon-gear'></span>
				</button>
			</td>";
			echo "</tr>";
		}
		echo "</table>";
	}
}

// / 调用
if (isset ( $_GET ['page'] )) {
	$page = ( int ) $_GET ['page'];
} else {
	$page = 1;
}
if (isset($_POST['order'])){
	$sql = "select * from Reservation order by ".$_POST['order'];
} else {
	$sql = "select * from Reservation order by idReservation desc";
}
$pager_option = array (
		"sql" => $sql,
		"PageSize" => 10,
		"CurrentPageID" => $page,
		"connect" => $connect 
);
if (isset ( $_GET ['numItems'] )) {
	$pager_option ['numItems'] = ( int ) $_GET ['numItems'];
}
//include 'connect.php';

 $pager = new ReservationPager ( $pager_option );
 $pager->showReservationList ();
if ($pager->isFirstPage) {
	$turnover = "<a class='pager'>FirstPage</a><a class='pager'>Prev</a>";
} else {
	$turnover = "<a class='pager' href='?page=1&numItems=" . $pager->numItems . "'>FirstPage</a><a class='pager' href='?page=" . $pager->PreviousPageID . "&numItems=" . $pager->numItems . "'>Prev</a>";
}
if ($pager->isLastPage) {
	$turnover .= "<a class='pager'>Next</a><a class='pager'>LastPage</a>";
} else {
	$turnover .= "<a class='pager' href='?page=" . $pager->NextPageID . "&numItems=" . $pager->numItems . "'>Next</a><a class='pager' href='?page=" . $pager->numPages . "&numItems=" . $pager->numItems . "'>LastPage</a>";
}
?>


<div class="delete">
	<input type="submit" id="delete" name="delete" value="Delete">
	<span id="responseRes" style="color:red;"></span>
</div>
</form>
<div class="pager">
	<p>
	<?php echo $turnover; ?>
	</p>
	<p>
		<?php echo "$pager->CurrentPageID / $pager->numPages"; ?>
	</p>
</div>

<div id="dialog-form" title="Modify reservation">
	<form id="formUpdate" method="post" action="submitList.php">
		<label for="idReservation">ID</label>
		<input type="number" name="ID" id="idReservation" class="text ui-widget-content ui-corner-all" />
		<label for="name">Name</label>
		<input type="text" name="name" id="name" class="text ui-widget-content ui-corner-all" />
		<label for="firstname">Firstname</label>
		<input type="text" name="firstname" id="firstname" class="text ui-widget-content ui-corner-all" />	
		<label for="sex">Sex</label>
		<input type="text" name="sex" id="sex" class="text ui-widget-content ui-corner-all" />
		<label for="dateIn">DateIn</label>
		<input type="date" name="dateIn" id="dateIn" class="text ui-widget-content ui-corner-all" />
		<label for="dateOut">DateOut</label>
		<input type="date" name="dateOut" id="dateOut" class="text ui-widget-content ui-corner-all" />
		<label for="price">Price</label>
		<input type="number" name="price" id="price" class="text ui-widget-content ui-corner-all" />
		<label for="Room_idRoom">Room</label>
		<input type="number" name="room" id="Room_idRoom" class="text ui-widget-content ui-corner-all" />
		<label for="dateRes">OperationTime</label>
		<input type="datetime" name="dateRes" id="dateRes" class="text ui-widget-content ui-corner-all" />
		<label for="state">State</label>
		<input type="text" name="state" id="state" class="text ui-widget-content ui-corner-all" />
		<label for="mail">Mail</label>
		<input type="email" name="mail" id="mail" class="text ui-widget-content ui-corner-all" />
		<label for="phone">Phone</label>
		<input type="tel" name="phone" id="phone" class="text ui-widget-content ui-corner-all" />
		<input type="hidden" name="formName" value="Reservation">
		<input type="hidden" name="update" value="Update">
		<span id="response" style="color:red;"></span>
	</form>
</div>
<script>
	$(function() {
		var allFields = $( [] );
		// var name = $( "#name" ),
		// 	email = $( "#email" ),
		// 	password = $( "#password" ),
		// 	allFields = $( [] ).add( name ).add( email ).add( password ),
		// 	tips = $( ".validateTips" );

		$( "input[type=submit], a, button" ).button();
		$( "#radio" ).buttonset();	

		var options = {
			url: "submitList.php", 
			target: "#responseRes",
						// success: showResponse 
			success: function () {
				window.location.reload();
			} 
		};

		
		




		$( "#dialog-form" ).dialog({
			autoOpen: false,
			height: 400,
			width: 350,
			modal: true,
			position: [333, 150],
			buttons: {
				"Modify": function() {
					var bValid = true;
					allFields.removeClass( "ui-state-error" );
					var options2 = {
						url: "submitList.php", 
						target: "#response",
									// success: showResponse 
						// success: function () {
						// 	window.location.reload();
						// } 
					};	
					$("#formUpdate").ajaxSubmit(options2);
				},
				Cancel: function() {
					$( this ).dialog( "close" );
					window.location.reload();
					
				}
			},
			close: function() {
				allFields.val( "" ).removeClass( "ui-state-error" );
				window.location.reload();
			}
		});


	


		$("button.update").click(function(){
			var idInput = $(this).val();
			$("input.text").each(function(){
				var name = $(this).attr("id");
				var str = "td#"+idInput+"."+name;
				var value = $(str).text();
				$(this).val(value);
			});
			$("#dialog-form").dialog("open");
			
		});

		$("#formRes").ajaxForm(options);
	});
	
</script>
</body>
</html>
