<?php
/*
 * jQuery File Upload Plugin PHP Example 5.14
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

// for real web server
$upload_url = 'http://'.$_SERVER['HTTP_HOST'];

// for localhost test
// $upload_url = 'http://'.$_SERVER['HTTP_HOST'].'/Projetdev';

$upload_dir = realpath(dirname($_SERVER['SCRIPT_FILENAME']).'/../../../../../');

$script_url = get_full_url();
// $script_url = dirname($_SERVER['SCRIPT_FILENAME']);
// echo "$script_url";
// $upload_dir = realpath("./Projetdev");
error_reporting(E_ALL | E_STRICT);

require('UploadHandler.php');


if (!($_GET['dir'] == null)) {
	$upload_url = $upload_url.'/image/'.$_GET['dir'].'/';
	$upload_dir = $upload_dir.'/image/'.$_GET['dir'].'/';
	$script_url = $script_url.'/?dir='.$_GET['dir'];
	$options = array('upload_dir' => $upload_dir,
 		'upload_url' => $upload_url,
 		'script_url' => $script_url);
	$upload_handler = new UploadHandler($options);
}else
	$upload_handler = new UploadHandler();


function get_full_url() {
	$https = !empty($_SERVER['HTTPS']) && strcasecmp($_SERVER['HTTPS'], 'on') === 0;
	return
	($https ? 'https://' : 'http://').
	(!empty($_SERVER['REMOTE_USER']) ? $_SERVER['REMOTE_USER'].'@' : '').
	(isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ($_SERVER['SERVER_NAME'].
		($https && $_SERVER['SERVER_PORT'] === 443 ||
			$_SERVER['SERVER_PORT'] === 80 ? '' : ':'.$_SERVER['SERVER_PORT']))).
	substr($_SERVER['SCRIPT_NAME'],0, strrpos($_SERVER['SCRIPT_NAME'], '/'));
}
