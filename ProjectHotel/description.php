<?php
session_start();

//cheak out client
if(!isset($_SESSION['userid'])){
	echo "<script language=\"javascript\">window.open(\"index.html\", \"_parent\");</script>";
	exit();
}
?>
<html>
<head>

	<meta charset="UTF-8">
	<title>Other Descriptions</title>
	<h1 style="text-align: center;">Descriptions</h1>

	<link rel="stylesheet" href="css/table.css">
	<script src="tricks/jquery-1.11.0.min.js"></script>
	<script src="tricks/jquery-migrate-1.2.1.js"></script>
	<script src="js/tableCheckbox.js"></script>
	<script src="tricks/jquery.form.js"></script>
	<link rel="stylesheet" type="text/css" href="tricks/jquery-ui-1.10.4.custom/css/smoothness/jquery-ui-1.10.4.custom.min.css">
	<script src="tricks/jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.min.js"></script>


	<?php 
	include 'connect.php'; 
	?>
</head>
<body>
<form id="formOrder" method="post" action="news.php">
	
	<div id="radio">
		<button type="submit" id="search" value="Search">Search</button>
		<span>&nbsp;Order By:&nbsp;</span>
		<input type="radio" id="radio1" name="order" value="idOtherDescriptions" /><label for="radio1">ID</label>
		<input type="radio" id="radio2" name="order" value="name" /><label for="radio2">Name</label>
		<input type="radio" id="radio3" name="order" value="link" /><label for="radio3">Link</label>
	</div>
	
</form>
	<form id="formDes" method="post" action="submitList.php">
	<input type="hidden" name="formName" value="OtherDescriptions">
<?php
require 'Pager.php';
//
class NewsPager extends Pager {
	public function showNewsList() {
		global $langue;
		$data = $this->getPageData ();
		$form = "
		<table>
		<tr>
			<th><input id='pickAll' type='checkbox' value='0'/></th>
			<th>ID</th>
			<th>Name</th>
			<th>Link</th>
			<th></th>
		</tr>
		";
		// 显示结果的代码
		// ......
		echo "$form";

		foreach ( $data as $row ) {
			echo "<tr>";
			echo "<td><input name='chkItem[]' type='checkbox' value=".$row['idOtherDescriptions']."></td>";
			foreach ( $row as $key => $value ) {
				echo "<td>";
				echo $value;
				echo "</td>";
			}
			echo "
			<td>
				<a class='update' name='update'  href=editDes.php?id=".$row['idOtherDescriptions']."&update=true>
					<span class='ui-icon ui-icon-gear'></span>
				</a>
			</td>";
			echo "</tr>";
		}
		echo "</table>";
	}
}

// / 调用
if (isset ( $_GET ['page'] )) {
	$page = ( int ) $_GET ['page'];
} else {
	$page = 1;
}
if (isset($_POST['order'])){
	$sql = "select idOtherDescriptions,name,link from OtherDescriptions order by ".$_POST['order'];
} else {
	$sql = "select idOtherDescriptions,name,link from OtherDescriptions order by idOtherDescriptions";
}
$pager_option = array (
		"sql" => $sql,
		"PageSize" => 10,
		"CurrentPageID" => $page 
);
if (isset ( $_GET ['numItems'] )) {
	$pager_option ['numItems'] = ( int ) $_GET ['numItems'];
}
//include 'connect.php';
 $pager = new NewsPager ( $pager_option );
 $pager->showNewsList ();
if ($pager->isFirstPage) {
	$turnover = "<a class='pager'>FirstPage</a><a class='pager'>Prev</a>";
} else {
	$turnover = "<a class='pager' href='?$getLang&page=1&numItems=" . $pager->numItems . "'>FirstPage</a><a class='pager' href='?$getLang&page=" . $pager->PreviousPageID . "&numItems=" . $pager->numItems . "'>Prev</a>";
}
if ($pager->isLastPage) {
	$turnover .= "<a class='pager'>Next</a><a class='pager'>LastPage</a>";
} else {
	$turnover .= "<a class='pager' href='?$getLang&page=" . $pager->NextPageID . "&numItems=" . $pager->numItems . "'>Next</a><a class='pager' href='?$getLang&page=" . $pager->numPages . "&numItems=" . $pager->numItems . "'>LastPage</a>";
}
?>
<div class="delete">
	<input type="submit" id="delete" name="delete" value="Delete">
</div>
</form>
<div class="pager">
	<p>
	<?php echo $turnover; ?>
	</p>
	<p>
		<?php echo "$pager->CurrentPageID / $pager->numPages"; ?>
	</p>
</div>
<span id="response"></span>
<script>
	$(function() {

		$( "input[type=submit], a, button" ).button();
		$( "#radio" ).buttonset();	

		var options = {
			url: "submitList.php", 
			target: "#response",
						// success: showResponse 
			success: function () {
				window.location.reload();
			} 
		};

		
		$("#formDes").ajaxForm(options);

					
	});
	
</script>
</body>
</html>
