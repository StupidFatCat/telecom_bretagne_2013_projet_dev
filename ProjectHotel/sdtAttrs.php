<?php
session_start();

//cheak out client
if(!isset($_SESSION['userid'])){
	echo "<script language=\"javascript\">window.open(\"index.html\", \"_parent\");</script>";
	exit();
}
?>
<!DOCTYPE html>
<html>	
	<head>
		<title>A Simple Page with CKEditor</title>
		<script src="tricks/jquery-1.11.0.min.js"></script>
		<script src="ckeditor/ckeditor.js"></script>
		<script src="tricks/jquery.form.js"></script>
		<script src="tricks/jquery.validate.js"></script>		
		<link rel="stylesheet" type="text/css" href="tricks/jquery-ui-1.10.4.custom/css/smoothness/jquery-ui-1.10.4.custom.min.css">
		<script src="tricks/jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.min.js"></script>
		<link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
		<link rel="stylesheet" href="css/font.css">
	</head>
	<body>
<?php
	
	include 'connect.php';
	//langue?
	$sql = "select name, schedule from Salon where idSalon = 1";
	$res = mysqli_query($connect,$sql);
	if ($row = mysqli_fetch_array($res,MYSQL_ASSOC)) {
		$schedule = $row['schedule'];
		$name = $row['name'];
	}

 ?>
		<h1>Salon Attributes</h1>
		<form id="editSalon" method="post" action="#">
			<p>
				<span class='edit'>name:</span> <input name="name" type="text" value=<?php echo "$name"; ?>>
			</p>
			<p>
				<span class='edit'>Schedule:</span>
				<textarea name="editor1" id="editor1" rows="10" cols="80">
					<?php echo "$schedule"; ?>
				</textarea>
				<script>
					CKEDITOR.replace( 'editor1' );
				</script>
			</p>
			<div class="submit">
				<input type="submit" value=Submit name="submit" onclick="CKupdate();"/>
				<a href="javascript:history.back(-1);">Back</a>
			</div>
			<p>
				<span id="response" style="color:red"></span>
			</p>

		</form>
		<script type="text/javascript">
			$(function() {
				$( "input[type=submit], a, button" ).button();
		
				var options = {
					url:"roomSubmit.php?id=sdtAttrs", 
					target: "#response",
					// success: showResponse 
				};
				$("#editSalon").ajaxForm(options);

				
			});

			function CKupdate(){
				for ( instance in CKEDITOR.instances )
					CKEDITOR.instances[instance].updateElement();
			}		
			function showResponse(responseText, statusText)  { 
			    // for normal html responses, the first argument to the success callback 
			    // is the XMLHttpRequest object's responseText property 
			 
			    // if the ajaxForm method was passed an Options Object with the dataType 
			    // property set to 'xml' then the first argument to the success callback 
			    // is the XMLHttpRequest object's responseXML property 
			 
			    // if the ajaxForm method was passed an Options Object with the dataType 
			    // property set to 'json' then the first argument to the success callback 
			    // is the json data object returned by the server 
			 
			    alert('status: ' + statusText + '\n\nresponseText: \n' + responseText + 
			        '\n\nThe output div should have already been updated with the responseText.'); 
			} 
		</script>
	</body>
</html>