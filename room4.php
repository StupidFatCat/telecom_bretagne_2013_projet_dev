<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/room4.css" />
        <link rel="stylesheet" href="css/footer.css" />
        <link rel="stylesheet" href="css/font.css" />
        <link rel="stylesheet" href="css/body.css" />
        <link rel="stylesheet" href="css/navposition.css" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

        <title>Projet dev</title>
    </head>

    <body marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" id="bod">
            
            <?php 
                include 'nav.php'; 
                include 'connect.php';

							

                if(isset($_GET['id'])) {
                    $i = (int) $_GET['id'];

				 $result = mysqli_query($con,"SELECT * FROM Room WHERE idRoom=$i");

            ?>
            <?php
                $dirname = 'image/'.$i.'/';
                $dir = opendir($dirname); 

                while($file = readdir($dir)) {
                    if($file != '.' && $file != '..' && !is_dir($dirname.$file))
                    {
                        $tab[] = $dirname.$file;
                    }
                }
                closedir($dir);
            ?>
            <section>
                <?php               
                    while($donnees = mysqli_fetch_array($result, MYSQL_BOTH)){
                ?>

                <div id="image">
                <div id="cf7" class="shadow">
                    <img class='opaque' src="<?php echo $tab[0];?>" />
                <?php 
                    for($j = 1;$j < 3; $j++){
                        $page= "photo" . (String)$j;
                ?>
                    <img src="<?php echo $tab[$j];?>" />
                <?php
                    }
                ?>
                </div>
                <p id="cf7_controls">
                    <span class="selected"><img src="image/boutton.png"></span>
                <?php 
                    for($j = 2; $j < 4; $j++){
                ?>
                        <span><img src="image/boutton.png"></span>
                <?php
                    }
                ?>
                </p>
            
                </div>
                <article>

                <div id="bloc">
                        <p>						
						<?php if($lang=='en'){
								echo $donnees['description_en'];
						?>
						<p>Tarif : <?php echo $donnees['price']; ?> &#8364 per night</p>
						<?php
						 }
						else{

								echo $donnees['description_fr'];
						?>
						<p>Tarif : <?php echo $donnees['price']; ?> &#8364 par nuit</p>
						<?php
						 }?></p>
                </div>
                <div id="reservation">
                    <a href="reservation.php" >Allez vers la page de réservation</a>
                </div>
                <?php
                    }      
                    }
                ?>
                </article>
        
            </section>




            <script>
                $(document).ready(function() {

                  $("#cf7_controls").on('click', 'span', function() {
                    $("#cf7 img").removeClass("opaque"); 

                    var newImage = $(this).index();

                    $("#cf7 img").eq(newImage).addClass("opaque");

                    $("#cf7_controls span").removeClass("selected");
                    $(this).addClass("selected");
                  });
                });
            </script>



                        
            
                
    </body>
    
</html>