<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, maximum-scale=1"/>
        <link rel="stylesheet" href="css/presentationMaison.css" />
        <link rel="stylesheet" href="css/footer.css" />
        <link rel="stylesheet" href="css/font.css" />
        <link rel="stylesheet" href="css/body.css" />
        <link rel="stylesheet" href="css/navposition.css" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <title>Projet dev</title>
    </head>

    <body marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" id="bod">
		
		<?php $lang = $_COOKIE["lang"];?>
	
        <?php 
                include 'nav.php'; 
                include 'connect.php';

			
			$result= mysqli_query($con, "SELECT * FROM House");
			$donnees= mysqli_fetch_array($result, MYSQL_BOTH);
			
			?>
            <?php
                $dirname = 'image/house/';
                $dir = opendir($dirname); 

                while($file = readdir($dir)) {
                    if($file != '.' && $file != '..' && !is_dir($dirname.$file))
                    {
                        $tab[] = $dirname.$file;
                    }
                }
                closedir($dir);
            ?>

        <div id="image">
        <div id="cf7" class="shadow">

          <img class='opaque' src="<?php echo $tab[0];?>" />

                <?php 
                    $i = 0;
                    foreach($tab as &$j){
                        $page= "photo" . (String)$j;
                        if($i!=0){
                ?> 
                        <img src="<?php echo $j;?>" />
                <?php
                        }
                        $i++;
                    }
                ?>

        </div>
        <p id="cf7_controls">
          <span class="selected"><img src="image/boutton.png"></span>
        <?php
            for($k = 1; $k < count($tab);$k++){
        ?>
            <span><img src="image/boutton.png"></span>
        <?php
        }
        ?>
        </p>
        </div>

            <section>
                <aside>

                    <p>
                        <?php echo $donnees['schedule'];?>
                    </p>
                </aside>
                <article>
                    <p>
						<?php if($lang=='en'){
								echo $donnees['description_en'];

						 }
						else{

								echo $donnees['description_fr'];
						 }?>
                    </p>
                </article>          
                
            </section>            
            <?php include("footer.php"); ?>            
            <?php include("blockreserver.php"); ?>


            <!--<?php //include("blockactualite.php"); ?>-->

        <script>
        
			$(document).ready(function() {

			  $("#cf7_controls").on('click', 'span', function() {
			    $("#cf7 img").removeClass("opaque"); 

			    var newImage = $(this).index();

			    $("#cf7 img").eq(newImage).addClass("opaque");

			    $("#cf7_controls span").removeClass("selected");
			    $(this).addClass("selected");
			  });
			});
		</script>
    </body>