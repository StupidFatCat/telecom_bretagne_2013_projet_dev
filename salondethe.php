<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, maximum-scale=1"/>
        <link rel="stylesheet" href="css/salondethe.css" />
        <link rel="stylesheet" href="css/footer.css" />
        <link rel="stylesheet" href="css/font.css" />
        <link rel="stylesheet" href="css/body.css" />
        <link rel="stylesheet" href="css/navposition.css" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <title>Projet dev</title>
    </head>

    <body marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" id="bod">
			
			<?php 
				include 'nav.php'; 
				include 'connect.php';

			
			$result= mysqli_query($con, "SELECT * FROM Salon");
			$donnees= mysqli_fetch_array($result, MYSQL_BOTH);
			
			?>
			
            <?php
                $dirname = 'image/sdt/';
                $dir = opendir($dirname); 

                while($file = readdir($dir)) {
                    if($file != '.' && $file != '..' && !is_dir($dirname.$file))
                    {
                        $tab[] = $dirname.$file;
                    }
                }
                closedir($dir);
            ?>
	        <div id="image">
	        <div id="cf7" class="shadow">
			
	          <img class='opaque' src="<?php echo $tab[0];?>" />
                <?php 
                    $i = 0;
                    foreach($tab as $j){
                        $page= "photo" . (String)$j;
                        if($i!=0){
                ?> 
                        <img src="<?php echo $j;?>" />
                <?php
                        }
                        $i++;
                    }
                ?>
	        </div>
	        <p id="cf7_controls">
	          <span class="selected"><img src="image/boutton.png"></span>
		<?php
            for($k = 1; $k < count($tab);$k++){
        ?>
            <span><img src="image/boutton.png"></span>
        <?php
        }
        ?>

	        </p>
        </div>
			<section>
				<aside>
					<p>
						<?php echo $donnees['schedule']?>
					</p>
				</aside>
				<article>
                    <h1><?php echo $donnees['name'];?></h1>
                    <p>
						<?php if($lang=='en'){
								echo $donnees['description_en'];

						 }
						else{

								echo $donnees['description_fr'];
						 }?>
                    </p>
				</article>
				<div id="menu">
                   <p>
                        <?php echo $donnees['menu'];?>
                    </p>
				</div>				
				<div id="galerie">
					<h1>Galerie</h1>
				<div id="miniature">
				
            <?php
                $dirname = 'image/menu/';
                $dir = opendir($dirname); 

                while($file = readdir($dir)) {
                    if($file != '.' && $file != '..' && !is_dir($dirname.$file))
                    {
                        $tab2[] = $dirname.$file;
                    }
                }
                closedir($dir);
            ?>

               <?php 
                    foreach($tab2 as $j){
                ?> 
                        <img src="<?php echo $j;?>" onmouseover=changeImagePlat("<?php echo $j;?>")>
                <?php
                    }
                ?>
				</div>
					<img src="<?php echo $tab2[0]?>" id="zoom">
				</div>
			</section>
			<?php include("footer.php"); ?>
			<?php include("blockreserver.php"); ?>
			<!--<?php //include("blockactualite.php"); ?>-->
			<script>
				function changeImagePlat(link)
				{
					document.getElementById("zoom").src=link;
				}				
       
				$(document).ready(function() {

				  $("#cf7_controls").on('click', 'span', function() {
				    $("#cf7 img").removeClass("opaque"); 

				    var newImage = $(this).index();

				    $("#cf7 img").eq(newImage).addClass("opaque");

				    $("#cf7_controls span").removeClass("selected");
				    $(this).addClass("selected");
				  });
				});
		
			</script>

    </body>
	
</html>