<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/check.css" />
        <link rel="stylesheet" href="css/footer.css" />
        <link rel="stylesheet" href="css/font.css" />
        <link rel="stylesheet" href="css/body.css" />
        <link rel="stylesheet" href="css/navposition.css" />
        <title>Projet dev</title>
    </head>

    <body id="bod">
			
			<?php 
				include 'nav.php'; 
				include 'connect.php';
			?>

            <?php
				if(!isset($_POST['chambre1']) && !isset($_POST['chambre2'])  && !isset($_POST['chambre3']) && !isset($_POST['chambre4'])){
            ?>
            <form class="form" method="post" action="reservation.php" >

                <p>Veuillez choisir une réservation</p>
                <div id="validation">
                    <input type="submit" value="Retour" class="validation"/>
                </div>
            </form>
            <?php
                }
                elseif(!isset($_POST['name']) || !isset($_POST['firstname']) || !isset($_POST['mail']) || !isset($_POST['tel'])){
            ?>
            <form class="form" method="post" action="recherche.php">

                <p>Veuillez renseignez tout les champs</p>
                <div id="validation">
				<?php
					for($l = 1; $l < 5; $l++){
						if(isset($_POST['in'.$l.'']) && isset($_POST['out'.$l.''])){
						?>
						<input type="hidden" value="<?php echo $_POST['in'.$l.''];?>" name="in<?php echo $i?>">
						<input type="hidden"  value="<?php echo $_POST['out'.$l.''];?>" name="out<?php echo $i?>">
						<input type="hidden"  value="<?php echo $_POST['chambre'.$l.''];?>" name="out<?php echo $i?>">
						<?php
						}
					}
				?>
                    <input type="submit" value="Retour" class="validation"/>
                </div>
            </form>
            <?php
                }
                elseif($_POST['name'] == null || $_POST['firstname'] == null || $_POST['mail'] == null || $_POST['tel'] == null){
            ?>
            <form class="form" method="post" action="recherche.php">

                <p>Veuillez renseignez tout les champs</p>
                <div id="validation">
				<?php
					for($l = 1; $l < 5; $l++){
						if(isset($_POST['in'.$l.'']) && isset($_POST['out'.$l.''])){
						?>
						<input type="hidden" value="<?php echo $_POST['in'.$l.''];?>" name="in<?php echo $i?>">
						<input type="hidden"  value="<?php echo $_POST['out'.$l.''];?>" name="out<?php echo $i?>">
						<input type="hidden"  value="<?php echo $_POST['chambre'.$l.''];?>" name="out<?php echo $i?>">
						<?php
						}
					}
				?>
                    <input type="submit" value="Retour" class="validation"/>
                </div>
            </form>
            <?php
                }
                elseif(!preg_match("/^[a-zA-Z ]*$/",$_POST['name']) || !preg_match("/^[a-zA-Z ]*$/",$_POST['firstname']) || !preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$_POST['mail']) || !preg_match("/^[0-9]*$/",$_POST['tel'])){
            ?>
            <form class="form" method="post" action="recherche.php">

                <p>Les champs ne sont pas correctemment remplis</p>
                <div id="validation">
				<?php
					for($l = 1; $l < 5; $l++){
						if(isset($_POST['in'.$l.'']) && isset($_POST['out'.$l.''])){
						?>
						<input type="hidden" value="<?php echo $_POST['in'.$l.''];?>" name="in<?php echo $i?>">
						<input type="hidden"  value="<?php echo $_POST['out'.$l.''];?>" name="out<?php echo $i?>">
						<input type="hidden"  value="<?php echo $_POST['chambre'.$l.''];?>" name="out<?php echo $i?>">
						<?php
						}
					}
				?>
                    <input type="submit" value="Retour" class="validation"/>
                </div>
            </form>
            <?php
                }

                else{
            ?>
            <div id='form'>
 
                <p> Récapitulatif de réservation: </p>
                <p>
				<?php
					$price = 0;
					
					for($l = 1; $l < 5; $l++){
						$result = mysqli_query($con,"SELECT * FROM Room WHERE  idRoom = $l");
						$donnees = mysqli_fetch_array($result, MYSQL_BOTH);
						if(isset($_POST['in'.$l.'']) && isset($_POST['out'.$l.'']) && isset($_POST['chambre'.$l.''])){
						?>	
								<p> Chambre <?php echo $l;?> <br/><p>
								<p> Du <?php echo $_POST['in'.$l.''] ?> au <?php echo $_POST['out'.$l.''] ?> <br/></p>
						<?php
								$price += (int) (((strtotime($_POST['out'.$l.'']) - strtotime($_POST['in'.$l.'']))/(60*60*24)))  * $donnees['price'];
						}
					}
				?>
				</p>
                <p>Nom: <?php echo $_POST['name']?></p>
                <p>Prenom: <?php echo $_POST['firstname'] ?></p>
                <p>E-mail: <?php echo $_POST['mail'] ?></p>
                <p>Tel: <?php echo $_POST['tel'] ?></p>
				<p>Prix : <?php echo $price;?>&euro;</p>
				
                <form method="post" action="validation.php">
				<?php
					for($l = 1; $l < 5; $l++){
						if(isset($_POST['in'.$l.'']) && isset($_POST['out'.$l.''])){
						?>
						<input type="hidden" value="<?php echo $_POST['in'.$l.''];?>" name="in<?php echo $l?>">
						<input type="hidden"  value="<?php echo $_POST['out'.$l.''];?>" name="out<?php echo $l?>">
						<input type="hidden"  value="<?php echo $_POST['chambre'.$l.''];?>" name="chambre<?php echo $l?>">

						<?php
						}
					}
				?>
                    
						<input type="hidden" value="<?php echo $_POST['name'];?>" name="name">
						<input type="hidden"  value="<?php echo $_POST['firstname'];?>" name="firstname">
						<input type="hidden"  value="<?php echo $_POST['mail'];?>" name="mail">
						<input type="hidden"  value="<?php echo $_POST['tel'];?>" name="tel">
						<input type="hidden"  value="<?php echo $price;?> " name="price">
			
				<div id="validation">
                    <input type="button" value="Retourner à la page de réservation" onclick="self.location.href='reservation.php'"/>
					<input type="submit" value="Valider" class="validation"/>
                </div>
				</form> 
			 </div>
             <?php
                }
            ?>

    </body>
	
</html>