<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/recherche.css" />
        <link rel="stylesheet" href="css/footer.css" />
        <link rel="stylesheet" href="css/font.css" />
        <link rel="stylesheet" href="css/body.css" />
        <link rel="stylesheet" href="css/navposition.css" />
        <title>Projet dev</title>
    </head>

    <body marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" id="bod">
			
			<?php include("nav.php"); ?>
			
			
			<?php
				if(!isset($_POST['chambre1']) && !isset($_POST['chambre2'])  && !isset($_POST['chambre3']) && !isset($_POST['chambre4'])){
			?>
			<form method="post" action="reservation.php">

				<p>Veuillez choisir au moins une chambre</p>
				<div id="validation">
					<input type="submit" value="Retour" class="validation"/>
				</div>
			</form>	
			<?php
				}
				else{
					$continu = 1;
					for($i = 1; $i < 5 && $continu; $i++){
						if(isset($_POST['chambre'.$i.''])){
							if(!isset($_POST['in'.$i.'']) || !isset($_POST['out'.$i.''])){
							?>
								<form method="post" action="reservation.php">

									<p>Vous n'avez pas donnée la date d'entrée et/ou de sortie des chambre sélectionnées</p>
									<div id="validation">
										<input type="submit" value="Retour" class="validation"/>
									</div>
								</form>	
							<?php
									$continu = 0;
							}
							elseif($_POST['in'.$i.''] == null || $_POST['out'.$i.''] == null){
							?>
								<form method="post" action="reservation.php">

									<p>Vous n'avez pas donnée la date d'entrée et/ou de sortie des chambre sélectionnées</p>
									<div id="validation">
										<input type="submit" value="Retour" class="validation"/>
									</div>
								</form>	
							<?php
									$continu = 0;
							}
							elseif((!preg_match('/(^[0-9]{2})\/([0-9]{2})\/([0-9]{4})/',(string)$_POST['in'.$i.'']) || strlen((string)$_POST['in'.$i.''])!=10) || (!preg_match('/(^[0-9]{2})\/([0-9]{2})\/([0-9]{4})/',(string)$_POST['out'.$i.'']) || strlen((string)$_POST['out'.$i.''])!=10)){
							?>
								<form method="post" action="reservation.php">
									<p>Pas la bonne forme</p>
									<div id="validation">
										<input type="submit" value="Retour" class="validation"/>
									</div>
								</form>
							<?php
									$continu = 0;
							}
							elseif(strtotime($_POST['in'.$i.'']) - strtotime($_POST['out'.$i.'']) > 0){
							?>
							<form method="post" action="reservation.php">

								<p>Une date de sorti précède celle d'entrée</p>
								<div id="validation">
									<input type="submit" value="Retour" class="validation"/>
								</div>
							</form>
							<?php
									$continu = 0;
							}
						}
					}
				}
				
				if($continu == 1){
				?>
					<form method="post" action="check.php">
					
				<?php
				
					for($l = 1; $l < 5; $l++){
						if(isset($_POST['in'.$l.'']) && isset($_POST['out'.$l.'']) && isset($_POST['chambre'.$l.''])){
						?>
						<input type="hidden" value="<?php echo $_POST['in'.$l.''];?>" name="in<?php echo $l?>">
						<input type="hidden"  value="<?php echo $_POST['out'.$l.''];?>" name="out<?php echo $l?>">
						<input type="hidden"  value="<?php echo $_POST['chambre'.$l.''];?>" name="chambre<?php echo $l?>">
						<?php
						}
					}
				?>
				<h1>Civilité</h1>
                <label>Nom: </label><input type="text" name="name"/><br/><br/>
                <label>Prénom: </label><input type="text" name="firstname"/><br/><br/>
                <label>Mail: </label><input type="text" name="mail"/><br/><br/>
                <label>Tel: </label><input type="text" name="tel"/><br/><br/>

					<div id="validation">
			 			<input type="submit" value="Valider" class="validation"/>
			 		</div>
					</form>
				<?php
				}
			?>
			
			
	
			
    </body>
	
</html>