<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/evenement.css" />
        <link rel="stylesheet" href="css/footer.css" />
        <link rel="stylesheet" href="css/font.css" />
        <link rel="stylesheet" href="css/body.css" />
        <link rel="stylesheet" href="css/navposition.css" />
        <title>Projet dev</title>
    </head>

    <body marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" id="bod">
		
			<?php 
				include 'nav.php'; 
				include 'connect.php';
			?>

			
			<?php
				
				if(isset($_GET['page'])) {
				    $i = $_GET['page'];
				}
				else{
					$i = 1;
				}
				$j = $i + 2;
				
			?>
			
			
			<?php $lang = $_COOKIE["lang"];?>
			<?php if($lang=='en'){
					$result = mysqli_query($con,"SELECT * FROM Event_en ORDER BY idEvent DESC");
			 }
			else{
					$result = mysqli_query($con,"SELECT * FROM Event_fr ORDER BY idEvent DESC");
			 }?>


			<section>
				<article>
				<?php			
					$k = 1;
					while($k < ($i-1)*8){
						$donnees = mysqli_fetch_array($result, MYSQL_BOTH);
						$k++;
					}
						
					$k = 0;	
					while($donnees = mysqli_fetch_array($result, MYSQL_BOTH)){
					if($k == 8)
						break;
				?>
				<div id="bloc">
					<div id='title'>
				<?php
						echo $donnees['title'] . "<br>";
				?>
					</div>
					<div id="contenu">
				<?php
						echo $donnees['content'] . "<br>";
						echo $donnees['date'] . "<br>";
				?>
					</div>
				</div>
				<?php		
					}
				?>
				</article>
		
				<aside>
					<?php
						$result = mysqli_query($con,"SELECT COUNT(*) as nb FROM Event_fr");
						$donnees = mysqli_fetch_array($result, MYSQL_BOTH);
						$j = 1;
						for($i = 1; $i < $donnees['nb'] ; $i+=8){
					?>
						<a href='evenement.php?page=<?php echo $j;?>'><?php echo $j;?></a>
					<?php
						$j++;
						}
					?>
				</aside>
			</section>
			<?php include("blockreserver.php"); ?>
			<!--<?php //include("blockarchive.php"); ?>-->
						

			
				
    </body>
	
</html>