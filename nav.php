<!DOCTYPE>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, maximum-scale=1"/>
    <link rel="stylesheet" href="css/nav.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

    <title>nav</title>
</head>
<body>
<?php $lang = $_COOKIE["lang"];?>
<?php if($lang=='en'){?>
<nav>
    <ul class="nav">
        <li><a href="index.php">La maison d'Anatole</a>
            <ul class="subs">
                <li><a href="presentationMaison.php">The home</a></li>
                <li><a href="room3.php">Our rooms</a></li>

            </ul>
        </li>
        <li><a href="salondethe.php">Le salon de thé</a>
        </li>
        <li><a href="#">La côte de Granit Rose</a>
            <ul class="subs">
                <li><a href="#">Port-Blanc</a></li>
                <li><a href="#">The surrounding</a></li>
            </ul>
        </li>
        <li><a href="actualite.php">News</a>
            <ul class="subs">
                <li><a href="evenement.php">Events</a></li>
            </ul>
        </li>
        <li><a href="contact.php">Location & Contact</a></li>
    </ul>
    <a href="#" id="pull">Menu</a>  
    <?php include("lang.php"); ?>

</nav>

<?php }
else{?>
<nav>
    <ul class="nav">
        <li><a href="index.php">La maison d'Anatole</a>
            <ul class="subs">
                <li><a href="presentationMaison.php">La maison</a></li>
                <li><a href="room3.php">Les chambres</a></li>

            </ul>
        </li>
        <li><a href="salondethe.php">Le salon de thé</a>
        </li>
        <li><a href="#">La côte de Granit Rose</a>
            <ul class="subs">
                <li><a href="#">Port-Blanc</a></li>
                <li><a href="#">Les environs</a></li>
            </ul>
        </li>
        <li><a href="actualite.php">Actualités</a>
            <ul class="subs">
                <li><a href="evenement.php">Evènement</a></li>
            </ul>
        </li>
        <li><a href="contact.php">Accès & Contact</a></li>
    </ul>
    <a href="#" id="pull">Menu</a>  
    <?php include("lang.php"); ?>

</nav>
<?php }?>

    <script>
        $(function() {
            var pull        = $('#pull');
                menu        = $('nav ul');
                menuHeight  = menu.height();

            $(pull).on('click', function(e) {
                e.preventDefault();
                menu.slideToggle();
            });

            $(window).resize(function(){
                var w = $(window).width();
                if(w > 700 && menu.is(':hidden')) {
                    menu.removeAttr('style');
                }
            });
        });
    </script>
</body>
</html>